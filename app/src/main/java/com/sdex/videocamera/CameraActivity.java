/*
 * Copyright 2015 Yuriy Mysochenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sdex.videocamera;

import android.content.Intent;
import android.hardware.Camera;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CameraActivity extends FragmentActivity implements MediaRecorder.OnInfoListener {

    private static final String TAG = "CameraActivity";

    public static String VIDEO_PATH = "video_path";

    private static final int INTERVAL_UPDATE_TIME = 40;

    private static final int UPDATE_RECORD_TIME = 0;
    private static final int SWITCH_CAMERA = 2;

    private static final int VIDEO_MAX_SIZE = 1024 * 1024 * 10; // 10 mb
    private static final int VIDEO_MAX_DURATION = 10 * 60 * 1000; // 10 min

    @Bind(R.id.camera_preview)
    CameraPreview mCameraPreview;
    @Bind(R.id.btn_switch_camera)
    ImageButton mSwitchCamera;
    @Bind(R.id.btn_shutter)
    ImageView mShutter;
    @Bind(R.id.btn_done)
    ImageButton mDone;
    @Bind(R.id.btn_cancel)
    ImageButton mCancel;
    @Bind(R.id.btn_play_video)
    ImageButton mPlayVideo;
    @Bind(R.id.time)
    TextView mRecordProgress;

    // we have to use deprecated camera because of
    // a new camera2 available only from API level 21
    private Camera mCamera;

    private int mNumberOfCameras;
    private int mCameraCurrentlyLocked;

    private MediaRecorder mMediaRecorder;
    boolean mRecording;
    private long mStartRecordTime;

    private Handler mHandler;

    private String mVideoPath;

    private static class MainHandler extends Handler {

        final WeakReference<CameraActivity> mActivityRef;

        public MainHandler(CameraActivity activity) {
            mActivityRef = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            CameraActivity activity = mActivityRef.get();
            if (activity == null) {
                removeMessages(msg.what);
                return;
            }

            switch (msg.what) {

                case UPDATE_RECORD_TIME: {
                    if (mActivityRef.get() != null) {
                        activity.updateRecordingTime();
                    }
                    break;
                }

                case SWITCH_CAMERA: {
                    activity.switchCamera();
                    break;
                }

                default:
                    Log.v(TAG, "Unhandled message: " + msg.what);
                    break;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_camera);
        ButterKnife.bind(this);

        mRecording = false;
        mHandler = new MainHandler(this);

        mVideoPath = getIntent().getStringExtra(VIDEO_PATH);
        if (TextUtils.isEmpty(mVideoPath)) {
//            mVideoPath = "/sdcard/video_" + System.currentTimeMillis() + ".mp4";
            mVideoPath = getFilesDir().getAbsolutePath() + File.separator + "video_" + System.currentTimeMillis() + ".mp4";
            showToast("Video path is empty. Use: " + mVideoPath);
        }

        detectCameras();
    }

    private void detectCameras() {
        // Find the total number of cameras available
        mNumberOfCameras = Camera.getNumberOfCameras();

        if (mNumberOfCameras == 0) {
            camerasNotFound();
        }

        // Find the ID of the default camera
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        for (int i = 0; i < mNumberOfCameras; i++) {
            Camera.getCameraInfo(i, cameraInfo);
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                mCameraCurrentlyLocked = i;
            }
        }
        if (!hasMultipleCameras()) {
            mSwitchCamera.setVisibility(View.GONE);
        }
    }

    private void camerasNotFound() {
        showToast("Can't find camera.");
        finish();
    }

    private boolean hasMultipleCameras() {
        return mNumberOfCameras > 1;
    }

    @OnClick(R.id.btn_shutter)
    void shutterClick() {
        if (mRecording) {
            stopRecording();
        } else {
            startRecording();
        }
    }

    @OnClick(R.id.btn_switch_camera)
    void switchCameraClick() {
        mHandler.sendEmptyMessage(SWITCH_CAMERA);
    }

    @OnClick(R.id.btn_done)
    void doneClick() {
        File videoFile = new File(mVideoPath);
        Intent result = new Intent();
        result.setData(Uri.fromFile(videoFile));
        setResult(RESULT_OK, result);
        finish();
    }

    @OnClick(R.id.btn_cancel)
    void cancelClick() {
        mRecordProgress.setVisibility(View.GONE);
        if (hasMultipleCameras()) {
            mSwitchCamera.setVisibility(View.VISIBLE);
        }
        mDone.setVisibility(View.GONE);
        mCancel.setVisibility(View.GONE);
        mPlayVideo.setVisibility(View.GONE);
        mShutter.setVisibility(View.VISIBLE);

        removeFile();
    }

    @OnClick(R.id.btn_play_video)
    void playVideoClick() {
        mPlayVideo.setVisibility(View.GONE);
        File file = new File(mVideoPath);
        Uri videoUri = FileProvider.getUriForFile(this, "com.sdex.videocamera", file);
        Intent intent = new Intent(Intent.ACTION_VIEW, videoUri);
        intent.setDataAndType(videoUri, "video/mp4");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(intent);
    }

    private void removeFile() {
        File videoFile = new File(mVideoPath);
        videoFile.delete();
    }

    private Camera getCameraInstance(int id) {
        Camera camera = null;
        try {
            camera = Camera.open(id);
            mCameraCurrentlyLocked = id;
            // enable autofocus
            Camera.Parameters params = camera.getParameters();
            if (params.getSupportedFocusModes().contains(
                    Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO)) {
                params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
            }
            camera.setParameters(params);

        } catch (Exception e) {
        }
        return camera;
    }

    private void switchCamera() {
        // Release this camera -> cameraCurrentlyLocked
        if (mCamera != null) {
            mCamera.stopPreview();
            mCameraPreview.setCamera(null);
            mCamera.release();
            mCamera = null;
        }

        // Acquire the next camera and request Preview to reconfigure parameters.
        mCamera = getCameraInstance((mCameraCurrentlyLocked + 1) % mNumberOfCameras);

        mCameraPreview.switchCamera(mCamera);

        // Start the preview
        mCamera.startPreview();
    }

    private void startRecording() {

        if (!prepareMediaRecorder()) {
            showToast("Recorder start failed.");
            return;
        }

        mMediaRecorder.start();
        mRecording = true;
        mStartRecordTime = System.currentTimeMillis();
        mHandler.sendEmptyMessageDelayed(UPDATE_RECORD_TIME, INTERVAL_UPDATE_TIME);
        updateUIAfterStart();
    }

    private void stopRecording() {
        updateUIAfterStop();
        mMediaRecorder.stop();
        releaseMediaRecorder();
        mRecording = false;
        mHandler.removeMessages(UPDATE_RECORD_TIME);

        stopPreview();
    }

    private void stopPreview() {
        mCamera.stopPreview();
    }

    private void updateUIAfterStop() {
        mRecordProgress.setVisibility(View.GONE);
        mDone.setVisibility(View.VISIBLE);
        mCancel.setVisibility(View.VISIBLE);
        mPlayVideo.setVisibility(View.VISIBLE);
        mShutter.setImageResource(R.drawable.btn_shutter_video);
        mShutter.setVisibility(View.INVISIBLE);
    }

    private void updateUIAfterStart() {
        mRecordProgress.setVisibility(View.VISIBLE);
        if (hasMultipleCameras()) {
            mSwitchCamera.setVisibility(View.GONE);
        }
        mDone.setVisibility(View.GONE);
        mCancel.setVisibility(View.GONE);
        mShutter.setImageResource(R.drawable.btn_shutter_video_recording);
    }

    private void updateRecordingTime() {
        mRecordProgress.setText(convertYoString(System.currentTimeMillis() - mStartRecordTime));
        mHandler.sendEmptyMessageDelayed(UPDATE_RECORD_TIME, INTERVAL_UPDATE_TIME);
    }

    private boolean prepareMediaRecorder() {
        mMediaRecorder = new MediaRecorder();

        mCamera.unlock();
        mMediaRecorder.setCamera(mCamera);

        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

        mMediaRecorder.setMaxDuration(VIDEO_MAX_DURATION);
        mMediaRecorder.setMaxFileSize(VIDEO_MAX_SIZE);

        mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);

        mMediaRecorder.setOutputFile(mVideoPath);

        mMediaRecorder.setOnInfoListener(this);
        mMediaRecorder.setPreviewDisplay(mCameraPreview.getHolder().getSurface());

        try {
            mMediaRecorder.prepare();
        } catch (IllegalStateException e) {
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            releaseMediaRecorder();
            return false;
        }
        return true;

    }

    @Override
    protected void onResume() {
        super.onResume();
        mCamera = getCameraInstance(mCameraCurrentlyLocked);
        mCameraPreview.setCamera(mCamera);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Because the Camera object is a shared resource, it's very
        // important to release it when the activity is paused.
        releaseMediaRecorder();
        releaseCamera();
    }

    private void releaseMediaRecorder() {
        if (mMediaRecorder != null) {
            mMediaRecorder.reset();
            mMediaRecorder.release();
            mMediaRecorder = null;
        }
    }

    private void releaseCamera() {
        if (mCamera != null) {
            mCameraPreview.setCamera(null);
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void onInfo(MediaRecorder mr, int what, int extra) {
        if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
            stopRecording();
            showToast("Max duration is reached. Stopping...");
        } else if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED) {
            stopRecording();
            showToast("Max size is reached. Stopping...");
        }
    }

    private String convertYoString(long millis) {
        long second = (millis / 1000) % 60;
        long minute = (millis / (1000 * 60)) % 60;
        return String.format("%02d:%02d", minute, second);
    }

    private void showToast(String message) {
        Toast.makeText(CameraActivity.this,
                message,
                Toast.LENGTH_LONG).show();
        Log.d(TAG, message);
    }

}
